package cz.springapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import cz.springapp.entity.Friend;

@Transactional(readOnly = true)
public interface FriendRepository extends JpaRepository<Friend, Long> {

}
