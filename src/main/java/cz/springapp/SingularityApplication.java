package cz.springapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
public class SingularityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SingularityApplication.class, args);
	}
}
