package cz.springapp.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class FriendPhoneId implements Serializable {
	
	private static final long serialVersionUID = 7824576225081541304L;

	@ManyToOne
	private Friend friend;
	
	@ManyToOne
	private PhoneNumber phone;

}
