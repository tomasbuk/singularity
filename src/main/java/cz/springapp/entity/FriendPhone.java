package cz.springapp.entity;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "FRIEND_PHONE")
@AssociationOverrides({ 
	@AssociationOverride(name = "pk.friend", joinColumns = @JoinColumn(name = "FRIEND_ID")),
	@AssociationOverride(name = "pk.phone", joinColumns = @JoinColumn(name = "PHONE_ID"))})
public class FriendPhone {
	
	@EmbeddedId
	private FriendPhoneId pk = new FriendPhoneId();
	
	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "END_DATE")
	private Date endDate;
	
//	@Transient
//	public Friend getFriend() {
//		return getPk().getFriend();
//	}
//	
//	public void setFriend(Friend friend) {
//		getPk().setFriend(friend);
//	}
//	
//	@Transient
//	public PhoneNumber getPhone() {
//		return getPk().getPhone();
//	}
//
//	public void setPhone(PhoneNumber phone) {
//		getPk().setPhone(phone);
//	}

	
	
}
