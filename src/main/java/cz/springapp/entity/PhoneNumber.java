package cz.springapp.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "PHONE_NUMBER")
public class PhoneNumber {
	
	@Id
	@Column(name = "PHONE_ID")
	private Long phoneId;
	
	@Column(name = "COUNTRY_CODE")
	private Long countryCode;
	
	@Column(name = "AREA_CODE")
	private Long areaCode;
	
	@Column(name = "PHONE_NUMBER")
	private Long phoneNumber;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.phone", cascade = CascadeType.ALL)
	private Set<FriendPhone> friends;

}
