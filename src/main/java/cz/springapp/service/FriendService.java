package cz.springapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.springapp.entity.Friend;
import cz.springapp.repository.FriendRepository;

@Service
public class FriendService {

	@Autowired
	private FriendRepository friendRepository;
	
	public List<Friend> getAllFriends() {
		List<Friend> friends = friendRepository.findAll();
		return friends;
	}
	
}
