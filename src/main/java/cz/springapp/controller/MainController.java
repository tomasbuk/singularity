package cz.springapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.springapp.entity.Friend;
import cz.springapp.service.FriendService;

@RequestMapping("/rest/api")
@RestController
public class MainController {
	
	@Autowired
	private FriendService friendService;
	
	
	@RequestMapping("/getAllFriends")
	public List<Friend> karel(HttpServletRequest req, HttpServletResponse res) {
		List<Friend> allFriends = friendService.getAllFriends();
		return allFriends;
	}

}
