package cz.springapp.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.springapp.IntegrationTest;
import cz.springapp.entity.Friend;
import cz.springapp.service.FriendService;

public class FriendServiceTest extends IntegrationTest {

	@Autowired
	private FriendService friendService;

	@Test
	public void testGetAllFriends() {
		List<Friend> allFriends = friendService.getAllFriends();
		assertThat(allFriends).isNotNull();
		assertThat(allFriends).isNotEmpty();
	}

}
