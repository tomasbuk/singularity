package cz.springapp;

import javax.transaction.Transactional;

import org.springframework.test.context.jdbc.Sql;

@Sql(scripts = {"/sql/drop.sql", "/sql/create.sql", "/sql/insert.sql"})
@Transactional // causes rollback of all the inserted test data when the test is over
public abstract class IntegrationTest extends BaseTest {

}
