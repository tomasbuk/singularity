# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Oracle XE configuration ####

##### Alter default 8080 port #####
1. sqlplus /nolog
2. connect
3. Exec DBMS_XDB.SETHTTPPORT(3010); // port 3010

##### Oracle Driver #####
**!BEWARE** You have to provide your oracle account credentials in order to be allowed to use their driver.
Following code needs to be inserted within <servers></servers> tag in ${M2_HOME}/settings.xml

	<server>
		<id>maven.oracle.com</id>
		<username>TomasBuk@seznam.cz</username>
		<password><!-- fill with our password --></password>
		<configuration>
		  <basicAuthScope>
			<host>ANY</host>
			<port>ANY</port>
			<realm>OAM 11g</realm>
		  </basicAuthScope>
		  <httpConfiguration>
			<all>
			  <params>
				<property>
				  <name>http.protocol.allow-circular-redirects</name>
				  <value>%b,true</value>
				</property>
			  </params>
			</all>
		  </httpConfiguration>
		</configuration>
	</server>
	
Then you can specify repositories and pluginRepositories within pom.xml

	<repositories>
		<repository>
			<id>maven.oracle.com</id>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<url>https://maven.oracle.com</url>
			<layout>default</layout>
		</repository>
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>maven.oracle.com</id>
			<url>https://maven.oracle.com</url>
		</pluginRepository>
	</pluginRepositories>
	
After that you can finally add the dependency itself
	
	<dependency>
		<groupId>com.oracle.jdbc</groupId>
		<artifactId>ojdbc7</artifactId>
		<version>${oracle.jdbc.version}</version>
	</dependency>



### Contribution guidelines ###

* // TODO

### Who do I talk to? ###

* // TODO